import { exec, OutputMode } from "https://deno.land/x/exec/mod.ts";

// 206,685 - hand crafted
// 209,625 - generated

const encoder = new TextEncoder();

let accumaltiveTime = 0;
for (let lp = 0; lp < 1000; lp += 1) {
  Deno.stdout.writeSync(encoder.encode(`${lp} `));
  const result = await exec("deno test", { output: OutputMode.Capture });
  const line = result.output.split("\n").filter((l) =>
    l.startsWith("test result:")
  )[0];

  accumaltiveTime += extractTime(line);
}

Deno.stdout.writeSync(encoder.encode(`Accumlative Time: ${accumaltiveTime}\n`));

function extractTime(line: string): number {
  let end = line.length - 1;

  while (line.substring(end, end + 2) != "ms") {
    end -= 1;
  }

  while (!isDigit(line[end])) {
    end -= 1;
  }
  let start = end;
  while (isDigit(line[start])) {
    start -= 1;
  }
  return parseInt(line.substring(start + 1, end + 1));
}

function isDigit(c: string): boolean {
  return (c.charCodeAt(0) >= "0".charCodeAt(0) &&
    c.charCodeAt(0) <= "9".charCodeAt(0));
}
