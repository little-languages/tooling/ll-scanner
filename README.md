# ll-scanner

This project is an implementation of a backtracking lexical automaton designed specifically for specifying and generating scanners or lexical analysers for programming and domain languages.  The project has the following features:

- Provides a syntax to define a programming language's lexical structure,
- Parsers the input file into an internal structure
- Creates the automaton for the internal structure
- Produces a scanner for a number of languages

The characteristics of the scanner:

- Given an input stream, the scanner will return a stream of tokens
- Each token in the returned stream includes the token's position with the input stream, the matched text and the token's symbol ID

## Input Syntax

The following EBNF grammar defines the syntax of a lexical definition.

```
Definition: 
    ["extend" LiteralString ";"]
    ["tokens" {Identifier "=" Expr ";"}]
    ["comments" {Comment ";"}]
    ["whitespace" Expr ";"]
    ["fragments" {Identifier "=" Expr ";"}]

Comment: Expr ["to" Expr ["nested"] ";"];

Expr: SequenceExpr {"|" SequenceExpr};

SequenceExpr: UnionExpr {UnionExpr};

UnionExpr: MinusExpr {"+" MinusExpr};

MinusExpr: NotExpr {"\" NotExpr};

NotExpr: ["!"] RangeExpr;

RangeExpr: Factor ["-" Factor];

Factor
  : "chr" "(" LiteralInt ")" 
  | LiteralCharacter 
  | LiteralString
  | "(" Expr ")"
  | "{" Expr "}"
  | "[" Expr "]"
  | Identifier
  ;
```

Using this grammar `ll-scanner`'s lexical structure is defined as follows:

```
tokens
    LiteralCharacter = chr(39) !chr(39) chr(39);
    LiteralInt = digit {digit};
    LiteralString = '"' {!'"'} '"';
    Identifier = alpha {alpha | digit};

comments
    "/*" to "*/" nested;
    "//" {!cr};

whitespace
    chr(0)-' ';

fragments
    digit = '0'-'9';
    alpha = 'a'-'z' + 'A'-'Z';
    cr = chr(10);
```

## Typescript Generator

The purpose of the generator is to create a fully function scanner written in Typescript for the lexical definition described in the passed file.  The characteristics of this implementation are:

* It will only generate code in the event that the input file is more recent than the generated files.  This allows it to be incorporated into a build pipeline and will only go through the generation process in the event that anything has changed.
* The generated code will be efficient.
* The generated code will be readable.

### Command line options

Usage

```
    ts {OPTIONS} FileName
```

Options:

* `--directory` | `-d` The directory where all of the generated will be placed.
* `--force` | `-f` Ignore the dates and force a rebuild of all generated code.
* `--lib-directory` | `-ld` The directory where the code generator's source fragements are located.
* `--verbose` | `-v` Lists all the files as they are created.

### Code generation strategy

* Multiple files are generated